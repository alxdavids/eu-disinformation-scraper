# Disinformation scraper

Scrapes fact checkers for current disinformation stories. Currently
supports:

- [EU vs Disinformation](https://euvsdisinfo.eu/)
- [Google Fact Check Explorer](https://toolbox.google.com/factcheck/explorer)

## Installation

```bash
git clone git@gitlab.com:alxdavids/eu-disinformation-scraper.git
yarn install
```

Scraping is run using `yarn scrape` with different flag options. See
below for more details.

## Output formats

### CSV

For documentation on the CSV headings, see <schema/csv.md>

```bash
export GOOGLE_API_KEY = <google_api_key>
yarn scrape --gak $GOOGLE_API_KEY -a --mad 3
```

### Postgresql

For documentation on the PostgreSQL DB schema, see <schema/db.md>

```bash
export DB_PASSWORD = <db_password>
yarn scrape --gak $GOOGLE_API_KEY --dbpwd $DB_PASSWORD -a --mad 3 -o psql
```

## Fact-checker options

### Running only one fact checker

You can replace the `-a` flag with either `-g` (Google fact checker
explorer) or `--eu` (EU vs disinformation) to specify
running only one of the fact checkers.

### Running for certain date ranges

Use the `start` and `end` date flags to specify a scraping period other
than from the present day. This is currently **not** supported by Google
Fact Check Explorer.

