module.exports = {
  automock: false,
  collectCoverage: true,
  collectCoverageFrom: ["src/*.ts"],
  coverageDirectory: "coverage",
  coverageReporters: ["json", "text", "html", "lcov"],
  modulePathIgnorePatterns: ["node_modules", "src/dom-manipulator"],
  setupFiles: ["<rootDir>/src/tests/setup.ts"],
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
  testEnvironment: "node",
  testMatch: [
    "**/__tests__/**/*.[jt]s?(x)",
    "**/?(*.)+(spec|test).[tj]s?(x)",
  ],
};
