# db-schema <!-- omit in toc -->

The DB schema below provides the tables and columns that holds
[ClaimReview](https://schema.org/ClaimReview) data scraped from fact
checkers.

## Table of contents <!-- omit in toc -->

- [Tables](#tables)
- [Data schema](#data-schema)
  - [`fact_checker`](#fact_checker)
  - [`claim_review`](#claim_review)
  - [`item_reviewed`](#item_reviewed)

## Tables

- `fact_checker`: Contains information on all of the observed fact
  checkers that claim reviews have been retrieved from.
- `claim_review`: All claim reviews that have been retrieved. Each
  review is linked to a corresponding fact checker in `fact_checker`.
- `item_reviewed`: The details on the disinformation item that is
  referred to in a claim review. There is a one-one correspondence
  between `item_reviewed` and `claim_review`.

## Data schema

We provide the columns for each of the tables above, along with their
expected datatype and other configuration options. Each column
corresponds to one of the headings defined in <csv.md>.

### `fact_checker`

- `id`
    ```json
    {
        type: UUID,primaryKey: true,
    }
    ```
- `name`: takes the value in `csv.fact_checker_name`
    ```json
    {
        type: TEXT,
        allowNull: false,
    }
    ```
- `domain`: takes the value in `csv.fact_checker_domain`
    ```json
    {
        type: TEXT,
        allowNull: false,
    }
    ```
- `lang`: takes the value in `csv.fact_checker_language`
    ```json
    {
        type: TEXT,
        allowNull: false,
    }
    ```

### `claim_review`

- `id`
    ```json
    {
        type: UUID,primaryKey: true,
    }
    ```
- `url`: takes the value in `csv.fact_checker_page_url`    
    ```json
    {
        type: TEXT,
        allowNull: false,
    }
    ```
- `page_title`: takes the value in `csv.fact_checker_page_title`
    ```json
    {
        type: TEXT,
        allowNull: true,
    }
    ```
- `claim_reviewed`: takes the value in `csv.claim_reviewed`
    ```json
    {
        type: TEXT,
        allowNull: false,
    }
    ```
- `claimant`: takes the value in `csv.claimant`
    ```json
    {
        type: TEXT,
        allowNull: false,
    }
    ```
- `date_published`: takes the value in `csv.claim_date_published`
    ```json
    {
        type: DATE,
        allowNull: false,
    }
    ```
- `review_rating`: takes the value in `csv.claim_review_rating`
    ```json
    {
        type: TEXT,
        allowNull: true,
    }
    ```
- `review_alternate_name`: takes the value in `csv.claim_review_alternate_name`
    ```json
    {
        type: TEXT,
        allowNull: true,
    }
    ```
- `other_appearances`: takes the value in `csv.claim_other_appearances`
    ```json
    {
        type: TEXT,
        allowNull: true,
    }
    ```
- `other_languages`: takes the value in `csv.claim_other_languages`
    ```json
    {
        type: TEXT,
        allowNull: true,
    }
    ```
- `fact_checker_id`: links to `id` in the `fact_checker` table
    ```json
    {
        type: UUID,
        allowNull: false,
    }
    ```

### `item_reviewed`

- `id`  
    ```json
    {
        type: UUID,primaryKey: true,
    }
    ```
- `url`: takes the value in `csv.claim_item_reviewed_url`
    ```json
    {
        type: TEXT,
        allowNull: true,
    }
    ```
- `type`: takes the value in `csv.claim_item_reviewed_type`
    ```json
    {
        type: TEXT,
        allowNull: true,
    }
    ```
- `media_type`: takes the value in `csv.claim_item_reviewed_media_type`
    ```json
    {
        type: TEXT,
        allowNull: true,
    }
    ```
- `author`: takes the value in `csv.claim_item_reviewed_author`
    ```json
    {
        type: TEXT,
        allowNull: true,
    }
    ```
- `lang`: takes the value in `csv.claim_item_reviewed_lang`
    ```json
    {
        type: TEXT,
        allowNull: true,
    }
    ```
- `claim_review_id`: links to the `id` column in the `claim_review`
  table
    ```json
    {
        type: UUID,
        allowNull: false,
    }
    ```

