# csv-schema

The following provides an explanation of the headers that are used in
the CSV output. The CSV output conveys information that is usually
produced in the [ClaimReview](https://schema.org/ClaimReview) by online
fact checkers.

- `fact_checker_page_url`
  - URL of the page where the fact check article is published       
- `fact_checker_page_title`
  - The title of the fact check page
- `fact_checker_name`
  - The name of the fact checker
- `fact_checker_domain`
  - The domain that the fact checker operates at, e.g.
    https://snopes.com
- `fact_checker_language`
  - The language that the fact checker operates in
- `claim_reviewed`
  - The details of the claim that is reviewed in the fact check
- `claim_claimant`
  - The name of the entity that has made the claim
- `claim_date_published`
  - The date that the claim review was published
- `claim_item_reviewed_url`
  - The URL of the item that initially raised the claim
- `claim_item_reviewed_type`
  - The identifier of the format that the claim originated in
- `claim_item_reviewed_media_type`
  - The type of media that was used to convey the claim
- `claim_item_reviewed_author`
  - The author of the claim
- `claim_item_reviewed_lang`
  - The language that the claim was written in
- `claim_review_rating`
  - Review rating of the claim by the fact checker (1 = false, 5 = true)
- `claim_review_alternate_name`
  - Textual field conveying the review rating
- `claim_other_appearances`
  - Other URLs that the claim was published at
- `claim_other_languages`
  - Other languages that the claim appeared in