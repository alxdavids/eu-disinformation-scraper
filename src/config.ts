import * as yargs from "yargs";
import {
  FactChecker,
  EUDisinformationFC,
  GoogleFC,
} from "./fact-checker";

// Setup the flags that are used
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const setupFlags = (): Record<string, any> =>
  yargs
    .option("start", {
      description:
        "The oldest date for reading entries. NOTE: this is only used by EU vs disinformation for now. The Google Fact Checker tool can only search from the current day.",
      alias: "dt",
      type: "string",
    })
    .option("end", {
      description:
        "The most recent date for reading entries. NOTE: this is only used by EU vs disinformation. The Google Fact Checker tool can only search from the current day.",
      alias: "de",
      type: "string",
    })
    .option("maxAgeDays", {
      description:
        "The maximum number of days worth of entries considered in each run of the scraper.",
      alias: "mad",
      type: "number",
    })
    .option("googleApiKey", {
      description: "Google fact checker API key",
      alias: "gak",
      type: "string",
    })
    .option("cron", {
      description:
        "Specifies whether the program should be run as a daily cron job",
      alias: "c",
      type: "boolean",
    })
    .option("all", {
      description: "Specifies whether all fact checkers should run",
      alias: "a",
      type: "boolean",
    })
    .option("google", {
      description:
        "Specifies whether the google fact checker should run",
      alias: "g",
      type: "boolean",
    })
    .option("euDisinf", {
      description:
        "Specifies whether the EU vs Disinformation fact checker should run",
      alias: "eu",
      type: "boolean",
    })
    .option("verbose", {
      description: "Shows all console logging when set",
      alias: "v",
      type: "boolean",
    })
    .option("sqllog", {
      description: "Shows all DB logging when set",
      alias: "sqllog",
      type: "boolean",
    })
    .option("output", {
      description:
        "Specifies the output format to use (options: csv, psql)",
      alias: "o",
      type: "string",
      default: "csv",
    })
    .option("db_passwd", {
      description: "Specifies the DB password that should be used",
      alias: "dbpwd",
      type: "string",
    })
    .help()
    .alias("help", "h").argv;

/**
 * FactChecker schema
 */
export const getFactCheckers = (
  argv: Record<string, any>, // eslint-disable-line @typescript-eslint/no-explicit-any
): Array<FactChecker> => {
  const EUDisinformationFactChecker = new EUDisinformationFC(
    "https://euvsdisinfo.eu/disinformation-cases/",
  );
  const GoogleFactChecker = new GoogleFC(
    "https://factchecktools.googleapis.com/v1alpha1/claims:search",
    ["coronavirus", "covid"],
    ["en", "de", "es", "pt"],
    [""],
    argv.googleApiKey,
  );
  const factCheckers: Array<FactChecker> = [];
  if (argv.a) {
    factCheckers.push(EUDisinformationFactChecker);
    if (!argv.googleApiKey) {
      console.log(
        "Ignoring Google fact checker, as no Google API key supplied.",
      );
    } else {
      factCheckers.push(GoogleFactChecker);
    }
  } else {
    if (argv.eu) {
      factCheckers.push(EUDisinformationFactChecker);
    }
    if (argv.g) {
      if (!argv.googleApiKey) {
        throw LOGGER.error(
          "Must specify Google API key for running Google fact checker",
        );
      }
      factCheckers.push(GoogleFactChecker);
    }
  }
  return factCheckers;
};

class Logger {
  verbose: boolean;
  dbLogging: boolean;
  constructor(v: boolean, dbLogging: boolean) {
    this.verbose = v;
    this.dbLogging = dbLogging;
  }
  log(s: string): void {
    if (this.verbose) {
      console.log(`\n${s}`);
    }
  }

  error(s: string): Error {
    const msg = `\n${s}`;
    console.error(msg);
    return new Error(msg);
  }

  setVerbose(v: boolean) {
    this.verbose = v;
  }

  setDBLogging(dbLogging: boolean) {
    this.dbLogging = dbLogging;
  }
}
export const LOGGER = new Logger(false, false);
