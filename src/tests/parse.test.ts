import {
  DisinformationBlob,
  getPageURL,
  parseDate,
  parseInnerPage,
  parseTable,
} from "../parse";
import * as fs from "fs";
import * as path from "path";
import fetch from "jest-fetch-mock";

describe("Parse DisinformationBlob", () => {
  test("check formatting", () => {
    const db = new DisinformationBlob(
      "outer-url",
      "title",
      "date-published",
      "first-appearance",
      "first-lang",
      "author-name",
      "author-type",
      "media-type",
      "rating",
      "urls",
    );
    expect(db.toArray()).toEqual([
      "outer-url",
      "title",
      "date-published",
      "first-appearance",
      "first-lang",
      "author-name",
      "author-type",
      "media-type",
      "rating",
      "urls",
    ]);
  });
});

describe("parsing dates", () => {
  test("correct 1", () => {
    const d = parseDate("14 Feb 2020");
    const dCheck = new Date("14 Feb 2020");
    expect(d).toStrictEqual(dCheck);
  });

  test("correct 2", () => {
    const d = parseDate("2020 Feb 14");
    const dCheck = new Date("14 Feb 2020");
    expect(d).toStrictEqual(dCheck);
  });
});

describe("Retrieve page URL", () => {
  const baseURL = "https://example.com/";
  const day = "14.02.2020";
  for (let i = 0; i < 10; i++) {
    test(`page ${i}`, () => {
      const url = getPageURL(baseURL, i, day);
      expect(url).toStrictEqual(
        `https://example.com/?offset=${i * 10}&date=${day}+-+${day}`,
      );
    });
  }
});

describe("Parse table", () => {
  beforeEach(() => fetch.resetMocks());
  const pageURL = "https://example.com/";
  test("bad status", async () => {
    fetch.mockResponseOnce("", { status: 404 });
    parseTable(pageURL)
      .then(() => {
        fail("This should not be reached");
      })
      .catch((e) => {
        expect(e.message).toStrictEqual(
          `Scraping failed for ${pageURL}`,
        );
      });
  });

  test("empty body", async () => {
    const body = "<html></html>";
    fetch.mockResponseOnce(body, { status: 200 });
    const urls = await parseTable(pageURL);
    expect(urls.length).toStrictEqual(0);
  });

  test("empty table", async () => {
    const body = fs
      .readFileSync(path.resolve(__dirname, "data", "table-empty.html"))
      .toString();
    fetch.mockResponseOnce(body, { status: 200 });
    const urls = await parseTable(pageURL);
    expect(urls.length).toStrictEqual(0);
  });

  test("real data (07-12-2020)", async () => {
    const body = fs
      .readFileSync(path.resolve(__dirname, "data", "table.html"))
      .toString();
    fetch.mockResponseOnce(body, { status: 200 });
    const urls = await parseTable(pageURL);
    expect(urls.length).toStrictEqual(3);
    expect(urls[0]).toStrictEqual(
      "https://euvsdisinfo.eu/report/elections-in-venezuela-were-free-and-fair/",
    );
    expect(urls[1]).toStrictEqual(
      "https://euvsdisinfo.eu/report/us-pushes-kuril-islands-residents-to-betray-their-homeland/",
    );
    expect(urls[2]).toStrictEqual(
      "https://euvsdisinfo.eu/report/lithuania-constantly-interferes-into-affairs-of-belarus/",
    );
  });
});

describe("parse inner page", () => {
  const ou = "https://example.com";
  test("bad status", () => {
    Object.assign(console, { error: jest.fn() });
    const body = fs
      .readFileSync(path.resolve(__dirname, "data", "inner0.html"))
      .toString();
    fetch.mockResponseOnce(body, { status: 404 });
    const res = parseInnerPage(ou);
    expect(res).toMatchObject({});
  });
  const expectedBlobs = [
    [
      "https://example.com",
      "Disinfo: US sanctions against Turkey are illegal",
      "2020-12-15",
      "https://sputniknews.gr/rosia/202012149153504-lavrof-paranomes-oi-amerikanikes-kyroseis-enantia-stin-tourkia-gia-tous-S-400/",
      "gre",
      "Sputnik Greece",
      "Organization",
      "NewsArticle",
      1,
      undefined,
    ],
    [
      "https://example.com",
      "Disinfo: Lithuanian judges broke the law in the case of January 13th",
      "2020-12-14",
      "https://lt.sputniknews.ru/russia/20201214/13952171/SK-zaochno-obvinil-sudey-iz-Litvy-za-prigovor-rossiyanam-po-delu-13-yanvarya.html",
      "rus",
      "Sputnik Lithuania - Russian",
      "Organization",
      "NewsArticle",
      1,
      "https://lt.sputniknews.ru/russia/20201214/13952171/SK-zaochno-obvinil-sudey-iz-Litvy-za-prigovor-rossiyanam-po-delu-13-yanvarya.html,https://sputniknews.lt/russia/20201214/13953202/Rusijos-TK-uz-akiu-apkaltino-Lietuvos-teisjus-uz-rusu-nuteisima-Sausio-13-osios-byloje.html",
    ],
    [
      "https://example.com",
      "Disinfo: Navalny could have poisoned himself with village moonshine",
      "2020-12-15",
      "https://www.vesti.ru/article/2498549",
      "rus",
      "vesti.ru",
      "Organization",
      "NewsArticle",
      1,
      "https://www.vesti.ru/article/2498549,https://regnum.ru/news/3140994.html,https://news.ru/society/razrabotchik-novichka-schitaet-chto-navalnyj-mog-otravitsya-samogonom/?utm_source=yxnews&utm_medium=desktop",
    ],
    [
      "https://example.com",
      "Disinfo: Poland has lost 130 billion euros from its membership in the EU throughout 16 years",
      "2020-12-15",
      "https://pl.sputniknews.com/pisza-dla-nas/2020121113501764-ile-naprawde-kosztuje-nas-unia-europejska-sputnik/",
      "pol",
      "Sputnik Poland",
      "Organization",
      "NewsArticle",
      1,
      undefined,
    ],
  ];
  for (let i = 0; i < 4; i++) {
    const fileName = `inner${i}.html`;
    test(`parse ${fileName}`, async () => {
      const body = fs
        .readFileSync(path.resolve(__dirname, "data", fileName))
        .toString();
      fetch.mockResponseOnce(body, { status: 200 });
      const db = await parseInnerPage(ou);
      if (!db) {
        fail("DisinformationBlob should not be undefined");
      }
      expect(db.toArray()).toEqual(expectedBlobs[i]);
    });
  }
});
