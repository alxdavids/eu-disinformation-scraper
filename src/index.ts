import * as path from "path";
import * as fs from "fs";
import {
  addOffset,
  parseDate,
  formatDate,
  isValidDate,
  pluralise,
} from "./utils";
import { getDatabase, DatabaseType } from "./db";
import {
  FactChecker,
  GoogleFC,
  DisinformationBlob,
  MissingClaimReviewError,
  BadlyFormattedClaimReviewError,
  MissingItemReviewedError,
} from "./fact-checker";
import { Spinner } from "./spinner";
import { CronJob } from "cron";
import { default as dateformat } from "dateformat";
import { setupFlags, getFactCheckers, LOGGER } from "./config";

const argv = setupFlags();
const FACT_CHECKERS = getFactCheckers(argv);
LOGGER.log(
  `Running the following fact checkers: ${JSON.stringify(
    FACT_CHECKERS.map((fc) => fc.name()),
    null,
    4,
  )}`,
);
const MAX_AGE_DAYS = argv.maxAgeDays || 3;
LOGGER.setVerbose(argv.v);
LOGGER.setDBLogging(argv.sqllog);
const DB_USER = "disinf_scraper";
const DB_PASSWORD = argv.db_passwd;
const DB_INSTANCE = getDatabase(
  argv.o as DatabaseType,
  DB_USER,
  DB_PASSWORD,
);

/**
 * Performs a page scrape for each fact checker from the start date and
 * back as many days as specified
 * @param fc
 * @param formattedDate
 * @param maxAgeDays
 */
async function write(
  fc: FactChecker,
  formattedDate: string,
  maxAgeDays: number,
): Promise<number> {
  let totalLength = 0;
  const fcSpinner = new Spinner(
    `Scraping ${fc.name()} for ${formattedDate} and ${maxAgeDays} ${pluralise(
      "day",
      maxAgeDays,
    )} previous.`,
  );
  fcSpinner.start();
  const baseURLs = fc
    .getAllowedSearchQueryModifiers()
    .map((sq) => `${fc.url}${sq}`)
    .map((url) => {
      return fc.getAllowedLanguageQueryModifiers().map((lq) => {
        return `${url}${lq}`;
      });
    })
    .reduce((acc, r) => acc.concat(r), [])
    .map((url) => {
      return fc.getAllowedHostsQueryModifiers().map((hq) => {
        return `${url}${hq}`;
      });
    })
    .reduce((acc, r) => acc.concat(r), []);
  const outputRows: Array<DisinformationBlob> = [];
  let foundResults = true;
  let page = 0;
  while (foundResults) {
    // Add extra modifiers
    const dateModifier = fc.getDateQueryModifier(
      parseDate(formattedDate),
      maxAgeDays,
    );
    const queryURLs = baseURLs.map(
      (url) =>
        `${addOffset(
          url,
          page,
        )}${dateModifier}${fc.getAPIKeyQueryModifier()}`,
    );

    // Run queries
    const promises = queryURLs.map(async (url) => fc.outerParser(url));
    const responses = await Promise.all(promises);
    const allResponses = responses.reduce(
      (acc, r) => acc.concat(r),
      [],
    );
    if (!allResponses || allResponses.length === 0) {
      foundResults = false;
      continue;
    }
    // filter out queryURLs without responses
    for (let i = 0; i < responses.length; i++) {
      if (!responses[i] || responses[i].length === 0) {
        baseURLs.splice(i, 1);
      }
    }

    // only allow certain URLs
    for (let i = 0; i < allResponses.length; i++) {
      const resp = allResponses[i] || {};
      if (!resp.url) {
        LOGGER.error(`Null response received`);
      }
      const innerURL = resp.url;
      let row: DisinformationBlob;
      try {
        row = await fc.innerParser(innerURL as string);
      } catch (e) {
        if (e instanceof MissingItemReviewedError) {
          LOGGER.log(`${e.message}`);
          continue;
        } else if (e instanceof MissingClaimReviewError) {
          LOGGER.log(`${e.message}`);
          continue;
        } else if (e instanceof BadlyFormattedClaimReviewError) {
          LOGGER.error(`${e.message}. Ignoring for now.`);
          continue;
        } else {
          throw LOGGER.error(e);
        }
      }
      if (row === undefined) {
        LOGGER.error(
          `An error occurred processing the page at ${innerURL}. Ignoring for now, see log for full details.`,
        );
        continue;
      } else if (fc instanceof GoogleFC) {
        row.title = resp.title || "";
        row.claimant = resp.claimant || "";
        row.itemReviewedLang = resp.itemReviewedLang || "";
      }
      outputRows.push(row as DisinformationBlob);
    }
    page++;
  }

  // Write rows to file
  if (outputRows.length > 0) {
    await DB_INSTANCE.write(outputRows, fc, formattedDate);
  }
  totalLength += outputRows.length;
  fcSpinner.succeed(
    `Scraped ${
      outputRows.length
    } rows from ${fc.name()} for ${formattedDate} (including previous ${maxAgeDays} ${pluralise(
      "day",
      maxAgeDays,
    )}).`,
  );
  return totalLength;
}

/**
 * Scrapes URLs from the different fact checkers that are setup. The
 * date parameter can be used to
 * @param maxAgeDays
 * @param date
 */
async function scrape(
  maxAgeDays: number,
  sDate?: string,
  eDate?: string,
): Promise<void> {
  const dateFormatter = "yyyy-mm-dd";

  // correctly setup dates
  let startDate = new Date();
  if (sDate !== undefined) {
    startDate = parseDate(sDate);
  } else {
    // set in the past
    startDate.setDate(startDate.getDate() - maxAgeDays);
  }
  const endDate = eDate !== undefined ? parseDate(eDate) : new Date();
  if (startDate > endDate) {
    throw LOGGER.error(
      `Incompatible start (${startDate.toDateString()}) and end (${endDate.toDateString()}) date chosen`,
    );
  } else if (startDate > new Date() || endDate > new Date()) {
    throw LOGGER.error(`Cannot choose a date in the future`);
  }

  // start scraping
  for (let i = 0; i < FACT_CHECKERS.length; i++) {
    const fc = FACT_CHECKERS[i];
    if (startDate == endDate || !fc.allowsDateRange) {
      const formattedDate = dateformat(new Date(), dateFormatter);
      LOGGER.log(
        `Running ${fc.name()} for most recent ${maxAgeDays} ${pluralise(
          "day",
          maxAgeDays,
        )} of entries`,
      );
      await write(fc, formattedDate, maxAgeDays);
    } else {
      LOGGER.log(
        `Running ${fc.name()} between ${startDate.toDateString()} and ${endDate.toDateString()}`,
      );
      const dateToRun = startDate;
      let runOnce = false;
      while (
        dateToRun < endDate ||
        (dateToRun <= endDate && !runOnce)
      ) {
        const mad = Math.min(
          maxAgeDays,
          endDate.getDate() - dateToRun.getDate(),
        );
        dateToRun.setDate(dateToRun.getDate() + mad);
        const formattedDate = dateformat(dateToRun, dateFormatter);
        await write(fc, formattedDate, mad);
        runOnce = true;
      }
    }
  }
}

if (!fs.existsSync(path.resolve(__dirname, "../", "output"))) {
  LOGGER.log("creating output directory");
  fs.mkdirSync(path.resolve(__dirname, "../", "output"));
}

// scrape and write
DB_INSTANCE.authenticate()
  .then((res: boolean) => {
    if (!res) {
      LOGGER.error(
        `Could not authenticate for chosen DB method: ${argv.o}`,
      );
      return;
    }
    return DB_INSTANCE.initModels()
      .then(() => {
        // start scraping
        if (!argv.cron) {
          return scrape(MAX_AGE_DAYS, argv.start, argv.end)
            .catch((e) => {
              LOGGER.error(e);
            })
            .then(() => DB_INSTANCE.close());
        } else {
          LOGGER.log("Running as daily cron job.");
          const cronJob = new CronJob("0 0 9 * * *", async () => {
            try {
              const dateToScrape = new Date();
              await scrape(MAX_AGE_DAYS, dateToScrape.toDateString());
            } catch (e) {
              throw LOGGER.error(`Cron job failed: ${e.message}`);
            }
          });
          LOGGER.log(
            "Next run on" + cronJob.nextDate().toDate().toDateString(),
          );
          // Start job
          if (!cronJob.running) {
            cronJob.start();
          }
        }
      })
      .catch((e) => {
        throw LOGGER.error(
          `Error during DB model initialisation: ${e.message}`,
        );
      });
  })
  .catch((e) => {
    LOGGER.error(`An error occurred: ${e.message}`);
  });
