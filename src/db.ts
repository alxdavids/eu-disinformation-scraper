import * as path from "path";
import * as csv from "fast-csv";
import * as fs from "fs";
import {
  DisinformationBlob,
  FactChecker as FactCheckerEntity,
} from "./fact-checker";
import { Sequelize, DataTypes, Model, Op } from "sequelize";
import { LOGGER } from "./config";
import { parseDate, isValidDate } from "./utils";

export enum DatabaseType {
  CSV = "csv",
  PSQL = "psql",
}

interface Database {
  authenticate(): Promise<boolean>;
  close(): Promise<void>;
  write(
    outputRows: Array<DisinformationBlob>,
    fc?: FactCheckerEntity,
    formattedDate?: string,
  ): Promise<void>;
  initModels(): Promise<void>;
}

/**
 * Retrieves the type of database functionality based on the provided type
 * @param type
 */
export function getDatabase(
  type: string,
  username?: string,
  password?: string,
): Database {
  switch (type) {
    case DatabaseType.CSV:
      return new CSV();
    case DatabaseType.PSQL:
      return new PSQL(username as string, password as string);
    default:
      throw LOGGER.error(
        "Invalid Database output type chosen: " + type,
      );
  }
}

class CSV implements Database {
  async close(): Promise<void> {
    return;
  }

  /**
   * Authenticates to the relevant DB instance
   * @param b
   */
  async authenticate(): Promise<boolean> {
    return true;
  }

  /**
   * Handles writing the output of the scraping
   * @param fc
   * @param outputType
   * @param outputRows
   * @param formattedDate
   */
  async write(
    outputRows: Array<DisinformationBlob>,
    fc: FactCheckerEntity,
    formattedDate: string,
  ): Promise<void> {
    let csvRows = [DisinformationBlob.csvHeaders()];
    csvRows = csvRows.concat(outputRows.map((row) => row.toArray()));
    const fileName = formattedDate.replace(/\-/g, "");
    const filePath = path.resolve(
      __dirname,
      "../",
      "output",
      `${fc.name()}-${fileName}.csv`,
    );
    fs.openSync(filePath, "w");
    csv.writeToPath(filePath, csvRows, { headers: true });
  }

  async initModels() {
    return;
  }
}

class PSQL implements Database {
  username: string;
  password: string;
  db: Sequelize;
  modelFactChecker?: FactChecker;
  modelClaimReview?: ClaimReview;
  modelItemReviewed?: ItemReviewed;

  constructor(username: string, password: string) {
    this.username = username;
    this.password = password;
    this.db = new Sequelize(
      `postgres://${username}:${password}@localhost:5432/postgres`,
      {
        logging: LOGGER.dbLogging,
      },
    );
  }

  async close(): Promise<void> {
    if (!this.db) {
      return;
    }
    this.db.close();
  }

  /**
   * Authenticates to the relevant DB instance
   * @param b
   */
  async authenticate(): Promise<boolean> {
    if (!this.db) {
      LOGGER.error("No DB found");
      return false;
    }
    return this.db
      .authenticate()
      .then(() => true)
      .catch((e) => {
        LOGGER.error(e);
        return false;
      });
  }

  /**
   * Handles writing the output of the scraping
   * @param fc
   */
  async write(outputRows: Array<DisinformationBlob>): Promise<void> {
    for (let i = 0; i < outputRows.length; i++) {
      const row = outputRows[i];
      const fcName = row.factCheckerName;
      const fcDomain = row.factCheckerURL;
      const fcLang = row.factCheckerLang;
      const [fcEntry, foundFC] = await FactChecker.findOrCreate({
        where: {
          name: fcName,
          domain: fcDomain,
          lang: fcLang,
        },
        defaults: {
          name: fcName,
          domain: fcDomain,
          lang: fcLang,
        },
      });
      if (foundFC) {
        LOGGER.log("Found duplicate for FC: " + fcEntry.id);
      }

      // create new ClaimReview entry
      const parsedDate = parseDate(row.datePublished);
      let datePublished;
      if (isValidDate(parsedDate)) {
        datePublished = parsedDate;
      } else {
        // Set the date to now if it can't be parsed
        datePublished = new Date();
      }
      const [crEntry, foundCR] = await ClaimReview.findOrCreate({
        where: {
          url: row.ou,
          fact_checker_id: fcEntry.id,
          claim_reviewed: row.claimReviewed,
        },
        defaults: {
          url: row.ou,
          page_title: row.title,
          claim_reviewed: row.claimReviewed,
          claimant: row.claimant,
          date_published: datePublished,
          review_rating: row.reviewRating,
          review_alternate_name: row.reviewAlternateName,
          other_appearances: row.listedURLs,
          other_languages: row.listedLangs,
          fact_checker_id: fcEntry.id,
        },
      });
      if (foundCR) {
        LOGGER.log("Found duplicate for CR: " + crEntry.id);
      }

      // create new ItemReviewed entry
      const [irEntry, foundIR] = await ItemReviewed.findOrCreate({
        where: {
          claim_review_id: crEntry.id,
        },
        defaults: {
          url: row.itemReviewedURL,
          type: row.itemReviewedType,
          media_type: row.itemReviewedMediaType,
          author: row.itemReviewedAuthor,
          lang: row.itemReviewedLang,
          claim_review_id: crEntry.id,
        },
      });
      if (foundIR) {
        LOGGER.log("Found duplicate for IR: " + irEntry.id);
      }
    }
    return;
  }

  async initModels(): Promise<void> {
    const sequelize = this.db;
    FactChecker.init(
      {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true,
        },
        name: {
          type: DataTypes.TEXT,
          allowNull: false,
        },
        domain: {
          type: DataTypes.TEXT,
          allowNull: false,
        },
        lang: {
          type: DataTypes.TEXT,
          allowNull: false,
        },
      },
      {
        sequelize,
        tableName: "fact_checker",
        modelName: "FactChecker",
      },
    );
    ClaimReview.init(
      {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true,
        },
        url: {
          type: DataTypes.TEXT,
          allowNull: false,
        },
        page_title: {
          type: DataTypes.TEXT,
          allowNull: true,
        },
        claim_reviewed: {
          type: DataTypes.TEXT,
          allowNull: false,
        },
        claimant: {
          type: DataTypes.TEXT,
          allowNull: false,
        },
        date_published: {
          type: DataTypes.DATE,
          allowNull: false,
        },
        review_rating: {
          type: DataTypes.TEXT,
          allowNull: true,
        },
        review_alternate_name: {
          type: DataTypes.TEXT,
          allowNull: true,
        },
        other_appearances: {
          type: DataTypes.TEXT,
          allowNull: true,
        },
        other_languages: {
          type: DataTypes.TEXT,
          allowNull: true,
        },
        fact_checker_id: {
          type: DataTypes.UUID,
          allowNull: false,
        },
      },
      {
        sequelize,
        tableName: "claim_review",
        modelName: "ClaimReview",
      },
    );
    ItemReviewed.init(
      {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true,
        },
        url: {
          type: DataTypes.TEXT,
          allowNull: true,
        },
        type: {
          type: DataTypes.TEXT,
          allowNull: true,
        },
        media_type: {
          type: DataTypes.TEXT,
          allowNull: true,
        },
        author: {
          type: DataTypes.TEXT,
          allowNull: true,
        },
        lang: {
          type: DataTypes.TEXT,
          allowNull: true,
        },
        claim_review_id: {
          type: DataTypes.UUID,
          allowNull: false,
        },
      },
      {
        sequelize,
        tableName: "item_reviewed",
        modelName: "ItemReviewed",
      },
    );
    await sequelize.sync();
  }
}

class FactChecker extends Model {
  id?: number;
  name?: string;
  domain?: string;
  lang?: string;
}

class ClaimReview extends Model {
  id?: number;
  url?: string;
  page_title?: string;
  claim_reviewed?: string;
  claimant?: string;
  date_published?: string;
  review_rating?: number;
  review_alternate_name?: string;
  other_appearances?: string;
  other_languages?: string;
  fact_checker_id?: number;
}

class ItemReviewed extends Model {
  id?: number;
  url?: string;
  type?: string;
  media_type?: string;
  author?: string;
  lang?: string;
  claim_review_id?: number;
}
