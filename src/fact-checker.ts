import fetch from "node-fetch";
import { default as HTMLDocument } from "./dom-manipulator/lib/index";
import { formatDate } from "./utils";
import { LOGGER } from "./config";

/**
 * Correspond to a single row in the CSV file
 */
export class DisinformationBlob {
  ou: string;
  title: string;
  factCheckerName: string;
  factCheckerURL: string;
  factCheckerLang: string;
  claimReviewed: string;
  claimant: string;
  datePublished: string;
  itemReviewedURL: string;
  itemReviewedType: string;
  itemReviewedMediaType: string;
  itemReviewedAuthor: string;
  itemReviewedLang: string;
  reviewRating: string;
  reviewAlternateName: string;
  listedURLs: string;
  listedLangs: string;

  constructor(
    ou: string,
    title: string,
    factCheckerName: string,
    factCheckerURL: string,
    factCheckerLang: string,
    claimReviewed: string,
    claimant: string,
    datePublished: string,
    itemReviewedURL: string,
    itemReviewedType: string,
    itemReviewedMediaType: string,
    itemReviewedAuthor: string,
    itemReviewedLang: string,
    reviewRating: string,
    reviewAlternateName: string,
    listedURLs: string,
    listedLangs: string,
  ) {
    this.ou = ou;
    this.title = title;
    this.factCheckerName = factCheckerName;
    this.factCheckerURL = factCheckerURL;
    this.factCheckerLang = factCheckerLang;
    this.claimReviewed = claimReviewed;
    this.claimant = claimant;
    this.datePublished = datePublished;
    this.itemReviewedURL = itemReviewedURL;
    this.itemReviewedType = itemReviewedType;
    this.itemReviewedMediaType = itemReviewedMediaType;
    this.itemReviewedAuthor = itemReviewedAuthor;
    this.itemReviewedLang = itemReviewedLang;
    this.reviewRating = reviewRating;
    this.reviewAlternateName = reviewAlternateName;
    this.listedURLs = listedURLs;
    this.listedLangs = listedLangs;
  }

  toArray(): Array<string> {
    return [
      this.ou,
      this.title,
      this.factCheckerName,
      this.factCheckerURL,
      this.factCheckerLang,
      this.claimReviewed,
      this.claimant,
      this.datePublished,
      this.itemReviewedURL,
      this.itemReviewedType,
      this.itemReviewedMediaType,
      this.itemReviewedAuthor,
      this.itemReviewedLang,
      this.reviewRating,
      this.reviewAlternateName,
      this.listedURLs,
      this.listedLangs,
    ];
  }

  static csvHeaders(): Array<string> {
    return [
      "fact_checker_page_url",
      "fact_checker_page_title",
      "fact_checker_name",
      "fact_checker_domain",
      "fact_checker_language",
      "claim_reviewed",
      "claim_claimant",
      "claim_date_published",
      "claim_item_reviewed_url",
      "claim_item_reviewed_type",
      "claim_item_reviewed_media_type",
      "claim_item_reviewed_author",
      "claim_item_reviewed_lang",
      "claim_review_rating",
      "claim_review_alternate_name",
      "claim_other_appearances",
      "claim_other_languages",
    ];
  }
}

export interface FactChecker {
  url: string;
  allowsDateRange: boolean;
  allowedSearches?: Array<string>;
  allowedLanguages?: Array<string>;
  allowedInnerHosts?: Array<string>;
  apiKey?: string;

  name: () => string;
  outerParser: (
    pageURL: string,
  ) => Promise<(Record<string, string> | null)[]>;
  innerParser: (ou: string) => Promise<DisinformationBlob>;
  getDateQueryModifier: (startDate: Date, maxAgeDays: number) => string;
  getAPIKeyQueryModifier: () => string;
  getAllowedSearchQueryModifiers: () => Array<string>;
  getAllowedLanguageQueryModifiers: () => Array<string>;
  getAllowedHostsQueryModifiers: () => Array<string>;
}

export class EUDisinformationFC implements FactChecker {
  url: string;
  allowsDateRange: boolean;

  constructor(url: string) {
    this.url = `${url}?`;
    this.allowsDateRange = true;
  }

  name(): string {
    return "eu-disinformation";
  }

  async outerParser(
    pageURL: string,
  ): Promise<(Record<string, string> | null)[]> {
    const resp = await fetch(pageURL);
    if (resp.status !== 200) {
      throw LOGGER.error(`Scraping failed for ${pageURL}`);
    }
    const body = await resp.text();
    const document = new HTMLDocument(body);
    const cells = document.getElementsByClassName(
      "disinfo-db-cell cell-title",
    );
    if (!cells) {
      throw LOGGER.error(`No table cells found for ${pageURL}`);
    }
    const filtered = cells.filter((c) => c.getAttribute("data-column"));
    const euVsDisURL = filtered.map((cell) => {
      return {
        url: cell.getElementsByTagName("a")[0].getAttribute("href"),
      };
    });
    return euVsDisURL as Array<Record<string, string>>;
  }

  async innerParser(ou: string): Promise<DisinformationBlob> {
    if (!ou) {
      throw LOGGER.error("No outer URL given");
    }
    const innerResp = await fetch(ou);
    if (innerResp.status !== 200) {
      throw LOGGER.error("Fetch failed for " + ou);
    }
    const innerBody = await innerResp.text();
    const innerDocument = new HTMLDocument(innerBody);
    const scripts = innerDocument.getElementsByTagName("script");
    const title = innerDocument.getElementsByClassName(
      "b-catalog__report-title",
    )[0].innerText;
    const metadata = scripts.filter(
      (tag) => tag.getAttribute("type") === "application/ld+json",
    );
    let claimReviewed;
    let datePublished;
    let itemReviewedURL;
    let itemReviewedLang;
    let authorName;
    let claimType;
    let mediaType;
    let reviewRating;
    let urls = "";
    let otherLangs = "";
    if (metadata.length > 0) {
      const md = metadata[0]; // only seems to be one metadata blob
      try {
        const parsed = JSON.parse(md.innerHTML);
        claimReviewed = parsed.claimReviewed;
        datePublished = parsed.datePublished;
        if (parsed.itemReviewed) {
          if (parsed.itemReviewed.firstAppearance) {
            itemReviewedURL = parsed.itemReviewed.firstAppearance.url;
            itemReviewedLang =
              parsed.itemReviewed.firstAppearance["inLanguage"][
                "alternateName"
              ];
            authorName =
              parsed.itemReviewed.firstAppearance.author["name"];
            claimType = parsed.itemReviewed["@type"];
            mediaType = parsed.itemReviewed.firstAppearance["@type"];
          }
          const appearances = parsed.itemReviewed.appearance;
          if (appearances && appearances.length > 0) {
            let first = true;
            appearances
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              .forEach((app: any) => {
                if (app.url) {
                  if (first) {
                    first = false;
                  } else {
                    urls = urls + ",";
                    otherLangs = otherLangs + ",";
                  }
                  urls = urls + decodeURI(app.url);
                  otherLangs =
                    otherLangs + app["inLanguage"]["alternateName"];
                }
              });
          }
        }
        if (parsed.reviewRating) {
          reviewRating = parsed.reviewRating.ratingValue;
        }
      } catch (err) {
        throw LOGGER.error(`Error occurred parsing metadata for ${ou}`);
      }
    } else {
      throw LOGGER.error(`No metadata found for ${ou}`);
    }
    return new DisinformationBlob(
      ou,
      title,
      "EU vs Disinformation",
      "https://euvsdisinfo.eu",
      "en",
      claimReviewed,
      authorName,
      datePublished,
      itemReviewedURL,
      claimType,
      mediaType,
      authorName,
      itemReviewedLang,
      reviewRating,
      reviewRating === 1 ? "False" : "Unknown",
      urls,
      otherLangs,
    );
  }

  getDateQueryModifier(startDate: Date, maxAgeDays: number): string {
    if (startDate === undefined) {
      throw LOGGER.error("Invalid date specified for query parameter");
    }
    const pastDate = new Date();
    pastDate.setDate(startDate.getDate() - maxAgeDays);
    return `&date=${formatDate(pastDate)}+-+${formatDate(startDate)}`;
  }

  getAPIKeyQueryModifier(): string {
    return "";
  }

  getAllowedSearchQueryModifiers(): Array<string> {
    return [""];
  }
  getAllowedLanguageQueryModifiers(): Array<string> {
    return [""];
  }
  getAllowedHostsQueryModifiers(): Array<string> {
    return [""];
  }
}

export class GoogleFC implements FactChecker {
  url: string;
  allowsDateRange: boolean;
  allowedSearches: Array<string>;
  allowedLanguages: Array<string>;
  allowedInnerHosts: Array<string>;
  apiKey: string;

  constructor(
    url: string,
    allowedSearches: Array<string>,
    allowedLanguages: Array<string>,
    allowedInnerHosts: Array<string>,
    apiKey: string,
  ) {
    this.url = `${url}?`;
    this.allowsDateRange = false;
    this.allowedSearches = allowedSearches;
    this.allowedLanguages = allowedLanguages;
    this.allowedInnerHosts = allowedInnerHosts;
    this.apiKey = apiKey;
  }

  name(): string {
    return "google";
  }

  async outerParser(
    pageURL: string,
  ): Promise<Array<Record<string, string> | null>> {
    const resp = await fetch(pageURL);
    if (resp.status !== 200) {
      throw LOGGER.error(
        `Scraping failed for ${pageURL} with status ${resp.status}`,
      );
    }
    const json = await resp.json();
    const claims = json.claims;
    if (!claims || claims.length === 0) {
      return [];
    }
    // TODO: use ClaimReview object
    return (
      claims
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .filter((claim: any) => claim.claimReview.length > 0)
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .map((claim: any) => {
          // TODO: why doesn't claim.claimReview[0] work??
          const rev = claim.claimReview.shift();
          return {
            url: rev.url,
            title: rev.title,
            languageCode: rev.languageCode,
          };
        })
    );
  }

  async innerParser(ou: string): Promise<DisinformationBlob> {
    if (!ou) {
      throw LOGGER.error("No outer URL given");
    }
    const innerResp = await fetch(ou);
    if (innerResp.status !== 200) {
      throw LOGGER.error("Fetch failed for " + ou);
    }
    const innerBody = await innerResp.text();
    const innerDocument = new HTMLDocument(innerBody);
    // get fact checker language
    let fcLangCode = "en";
    const htmlTag = innerDocument.getElementsByTagName("html")[0];
    if (htmlTag) {
      fcLangCode = htmlTag.getAttribute("lang") || "en";
    }

    // parse ClaimReview
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let claimReview: Record<string, any>;
    try {
      const scripts = innerDocument.getElementsByTagName("script");
      const metadataTags = scripts.filter(
        (tag) => tag.getAttribute("type") === "application/ld+json",
      );
      claimReview = metadataTags
        .map((md) => JSON.parse(md.innerHTML))
        .filter((json) => json["@type"] === "ClaimReview")[0];
    } catch (e) {
      throw new BadlyFormattedClaimReviewError(ou);
    }
    if (!claimReview) {
      throw new MissingClaimReviewError(ou);
    } else if (!claimReview.itemReviewed) {
      throw new MissingItemReviewedError(ou);
    }
    // parse itemReviewed
    const [
      itemReviewedURL,
      itemReviewedType,
      itemReviewedMediaType,
      itemReviewedAuthor,
      itemReviewedAppearances,
    ] = this.parseItemReviewed(claimReview);
    const ouHostname = new URL(ou).hostname;
    return new DisinformationBlob(
      ou,
      "title", // title set afterwards
      claimReview.author.name || ouHostname,
      claimReview.author.url || ouHostname,
      fcLangCode,
      claimReview.claimReviewed,
      "claimant", // claimant set afterwards
      claimReview.datePublished,
      itemReviewedURL !== ou ? (itemReviewedURL as string) : "",
      itemReviewedType as string,
      itemReviewedMediaType as string,
      itemReviewedAuthor as string,
      "languageCode", // language set afterwards
      claimReview.reviewRating.ratingValue,
      claimReview.reviewRating.reviewAlternateName,
      (itemReviewedAppearances as Array<Record<string, string>>)
        .map((appearance: Record<string, string>) => appearance.url)
        .join(","),
      "",
    );
  }

  getDateQueryModifier(startDate: Date, maxAgeDays: number): string {
    return `&maxAgeDays=${maxAgeDays}`;
  }

  getAPIKeyQueryModifier(): string {
    return `&key=${this.apiKey}`;
  }

  getAllowedHostsQueryModifiers(): Array<string> {
    if (
      this.allowedInnerHosts.length === 1 &&
      this.allowedInnerHosts[0] === ""
    ) {
      return [""];
    }
    return this.allowedInnerHosts.map(
      (host) => `&reviewPublisherSiteFilter=${host}`,
    );
  }

  getAllowedSearchQueryModifiers(): Array<string> {
    if (
      this.allowedSearches.length === 1 &&
      this.allowedSearches[0] === ""
    ) {
      return [""];
    }
    return this.allowedSearches.map((search) => `&query=${search}`);
  }

  getAllowedLanguageQueryModifiers(): Array<string> {
    if (
      this.allowedLanguages.length === 1 &&
      this.allowedLanguages[0] === ""
    ) {
      return [""];
    }
    return this.allowedLanguages.map((lang) => `&languageCode=${lang}`);
  }

  parseItemReviewed(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    claimReview: Record<string, any>,
  ): Array<string | Array<Record<string, string>>> {
    const itemReviewed = claimReview.itemReviewed;
    let itemReviewedURL = "";
    let itemReviewedType = itemReviewed
      ? itemReviewed["@type"]
      : "Claim";
    let itemReviewedMediaType = "";
    let itemReviewedAuthor = "";
    let itemReviewedAppearances = [];
    if (typeof itemReviewed === "string") {
      itemReviewedURL = itemReviewed;
      itemReviewedType = "Claim";
      itemReviewedAuthor = itemReviewed;
    } else if (typeof itemReviewed === "object") {
      // console.log(`\n${JSON.stringify(itemReviewed, null, 4)}`);
      const appearances = itemReviewed["appearance"];
      if (typeof appearances === "object") {
        if (appearances.length > 0) {
          itemReviewedAppearances = appearances;
          itemReviewedURL = appearances[0].url;
          itemReviewedMediaType = appearances[0].type;
        } else {
          itemReviewedURL = appearances.url;
          itemReviewedMediaType =
            appearances.type || appearances["@type"];
        }
      } else {
        itemReviewedURL = itemReviewed.url || itemReviewedURL;
      }
      const author = itemReviewed["author"];
      if (author) {
        itemReviewedAuthor = author.name || itemReviewedAuthor;
        itemReviewedURL = itemReviewedURL || author.sameAs;
      }
    }
    return [
      itemReviewedURL,
      itemReviewedType,
      itemReviewedMediaType,
      itemReviewedAuthor,
      itemReviewedAppearances,
    ];
  }
}

export class MissingItemReviewedError extends Error {
  constructor(url: string) {
    super(`No ItemReviewed object for ${url}`);
  }
}

export class MissingClaimReviewError extends Error {
  constructor(url: string) {
    super(`Missing ClaimReview object for ${url}`);
  }
}

export class BadlyFormattedClaimReviewError extends Error {
  constructor(url: string) {
    super(`Badly formatted ClaimReview object for ${url}`);
  }
}
