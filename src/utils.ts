import { default as dateformat } from "dateformat";

/**
 * Parses a string into a valid date for the EU vs Disinformation URL
 * query params
 * @param s
 */
export function parseDate(s: string): Date {
  return new Date(Date.parse(s));
}

/**
 * Formats a date into dd.mm.yyyy format
 * @param d
 */
export function formatDate(d: Date): string {
  const format = "dd.mm.yyyy";
  return dateformat(d, format);
}

/**
 * Returns the full URL for processing each page of table entries
 * @param baseURL
 * @param page
 * @param day
 */
export function addOffset(baseURL: string, page: number): string {
  const offset = page * 10;
  return `${baseURL}&offset=${offset}`;
}

/**
 * Checks if the date is valid
 * @param d
 */
export function isValidDate(d: Date): boolean {
  return d instanceof Date && !isNaN(d.getTime());
}

/**
 * really simple pluralisation
 * @param s
 * @param n
 */
export function pluralise(s: string, n: number): string {
  return n === 1 ? s : `${s}s`;
}
